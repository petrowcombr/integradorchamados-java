package br.com.everton.petrow.chamados;

public class Main {

    /*
    ATENÇÃO! É necessário importar o certificado SSL da página https://www.petrow.com.br 
    para que você consiga se comunicar com o nosso servidor através de uma aplicação Java WEB
    Baixe o certificado gerando o arquivo ".cer" e importe o mesmo usando o comando abaixo:
    keytool -import -alias petrow.com.br -file petrowcombr.cer -keystore cacerts
     */
    public static void main(String[] args) {
        // exemplo de uso de algumas das soluções
        try {
            String locale = "pt"; // informe o seu locale (pt / es / en)
            String login = "petrow"; // informe o seu login
            String password = "e2407s"; // informe a sua senha

            ////////////////////////////////////////////////////////////////////
            // gerar o token
            ////////////////////////////////////////////////////////////////////
            String token = Token.gerar("https://www.petrow.com.br/ws/Token", locale, login, password);

            System.out.println("TOKEN GERADO: " + token);
            System.out.println();

            ////////////////////////////////////////////////////////////////////
            // abrir um chamado
            ////////////////////////////////////////////////////////////////////
            String chamado = Chamado.abrir("https://www.petrow.com.br/ws/Ticket", locale, login, token, 1, 1, 1, "nome", "email", "telefone", "titulo", "descrição");

            if (chamado != null) {
                System.out.println("CHAMADO ABERTO: " + chamado);
                System.out.println();
            } else {
                System.out.println("ERRO: " + Chamado.getErro());
            }

        } catch (Exception e) {
            System.out.println("ERRO: " + e.getMessage());
        }
    }
}
