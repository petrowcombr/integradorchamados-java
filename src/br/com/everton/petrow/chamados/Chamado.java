package br.com.everton.petrow.chamados;

import java.net.URLEncoder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONObject;

public class Chamado {
    
    private static String erro;
    
    public static String getErro() {
        return erro;
    }

    public static String abrir(String url, String locale, String login, String token, long department, long subject, long user, String name, String email, String phone, String title, String description) throws Exception {
        erro = "";
        
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/octet-stream");
        RequestBody body = RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url(url + "/?locale=" + locale + "&login=" + URLEncoder.encode(login, "UTF-8") + "&token=" + token + "&action=open" + "&department=" + department + "&subject=" + subject + "&user=" + user + "&name=" + URLEncoder.encode(name, "UTF-8") + "&email=" + URLEncoder.encode(email, "UTF-8") + "&phone=" + URLEncoder.encode(phone, "UTF-8") + "&title=" + URLEncoder.encode(title, "UTF-8") + "&description=" + URLEncoder.encode(description, "UTF-8"))
                .post(body)
                .addHeader("cache-control", "no-cache")
                .build();

        Response response;
        
        response = client.newCall(request).execute();

        if (response.code() == 200) {
            JSONObject json = new JSONObject(response.body().string().trim());

            if (json.getString("status").equals("OK")) {
                response.close();
                
                return json.getString("id");
            } else {
                // erro
                for (int i = 0; i < json.getJSONObject("errors").names().length(); i++) {
                    erro += "[" + json.getJSONObject("errors").names().getString(i) + "] " + json.getJSONObject("errors").get(json.getJSONObject("errors").names().getString(i)) + "\n";
                }
            }
        } else {
            erro += "[" + response.code() + "] " + response.message() + " - " + url;
        }
        
        response.close();

        return null;
    }
}
